﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment.Data_Access;
using Assignment.Models;
using Google.Apis.Storage.v1.Data;
using Google.Cloud.Storage.V1;

namespace Assignment.Controllers
{
    public class AudiosController : Controller
    {
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost, Authorize]
        public ActionResult Create(Audio p, HttpPostedFileBase file)
        {
            //upload image related to product on the bucket
            try
            {
                var storage = StorageClient.Create();
                string link = "";
                using (var f = file.InputStream)
                {
                    var filename = Guid.NewGuid() + System.IO.Path.GetExtension(file.FileName);
                    var storageObject = storage.UploadObject("pfcbucket", filename, null, f);
                    

                    if (null == storageObject.Acl)
                    {
                        storageObject.Acl = new List<ObjectAccessControl>();
                    }
                    UserRepository ur = new UserRepository();
                    storageObject.Acl.Add(new ObjectAccessControl()
                    {
                        Bucket = "pfcbucket",
                        Entity = $"user-" + p.audioreceiver,
                        Role = "READER", //READER
                    });
                    var updatedObject = storage.UpdateObject(storageObject, new UpdateObjectOptions()
                    {
                        // Avoid race conditions.
                        IfMetagenerationMatch = storageObject.Metageneration,
                    });

                    //store details in a relational db including the filename/link
                    link = "https://storage.cloud.google.com/" + storageObject.Bucket + "/" + filename;
                    p.medialink = link;
                    p.audioowner = User.Identity.Name;
                    AudioRepository pr = new AudioRepository();
                    pr.AddAudio(p.audioname, p.audiodesc, p.audioowner, p.audioreceiver, p.medialink);

                    CacheRepository cr = new CacheRepository();
                    cr.UpdateCache(pr.GetAudios(User.Identity.Name));
                    PubSubRepository ps = new PubSubRepository();
                    ps.AddToEmailQueue(p);
                    new LogsRepository().WriteLogEntry("Audio: " + p.audioname + " created by " + User.Identity.Name + " at " + DateTime.Now + " with recipient " + p.audioreceiver);
                    ViewBag.Message = "Product created successfully";
                } 
            }
            catch (Exception ex)
            {
                new LogsRepository().LogError(ex);
                ViewBag.Error = "Product failed to be created; " + ex.Message;
            }

            return RedirectToAction("Index","Audios");
        }
        public Audio GetAudioyByID(int id)
        {
            Audio property = new Audio();
            AudioRepository pr = new AudioRepository();
            property = pr.GetAudio(id);
            return property;
        }
        [Authorize]
        public ActionResult Details(int id)
        {
            //upload image related to product on the bucket
            try
            {
                return View("Details", GetAudioyByID(id));
            }
            catch (Exception ex)
            {
                new LogsRepository().LogError(ex);
                ViewBag.Error = "Product Not Found " + ex.Message;
            }

            return RedirectToAction("Index", "Audios");
        }

        [Authorize]
        // GET: Products
        public ActionResult Index()
        {
            CacheRepository cr = new CacheRepository();
            var Audios = cr.GetaudiosFromCache();
            return View("Index", Audios);
        }
        [Authorize]
        public ActionResult Delete(int id)
        {
            AudioRepository pr = new AudioRepository();
            CacheRepository cr = new CacheRepository();
            pr.DeleteAudio(id);
            pr.GetAudios(User.Identity.Name);
            cr.UpdateCache(pr.GetAudios(User.Identity.Name));
            new LogsRepository().WriteLogEntry("Audio with id " + id +" deleted by " + User.Identity.Name + " at " + DateTime.Now);
            return RedirectToAction("Index","Audios");
        }

        public ActionResult UpdateCache()
        {
            AudioRepository pr = new AudioRepository();
            CacheRepository cr = new CacheRepository();
            var Audios = pr.GetAudios(User.Identity.Name);
            cr.UpdateCache(Audios);
            return RedirectToAction("Index", "Audios");
        }


        [HttpPost, Authorize]
        public ActionResult DeleteAll(int[] ids)
        {
            //1. Requirement when opening a transaction: Connection has to be opened

            AudioRepository pr = new AudioRepository();
            pr.MyConnection.Open();

            pr.MyTransaction = pr.MyConnection.BeginTransaction(); //from this point onwards all code executed against the db will remain pending

            try
            {
                foreach (int id in ids)
                {
                    pr.DeleteAudio(id);
                }

                pr.MyTransaction.Commit(); //Commit: you are confirming the changes in the db
            }
            catch (Exception ex)
            {
                //Log the exception on the cloud
                new LogsRepository().LogError(ex);
                pr.MyTransaction.Rollback(); //Rollback: it will reverse all the changes done within the try-clause in the db
            }

            pr.MyConnection.Close();

            return RedirectToAction("Index");
        }
    }
}
