﻿using Assignment.Data_Access;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Assignment.Controllers
{
    public class CronController : Controller
    {
        // GET: Cron
        public ActionResult RunEveryMinute()
        {

            PubSubRepository psr = new PubSubRepository();
            psr.DownloadEmailFromQueueAndSend();
            new LogsRepository().WriteLogEntry("Emails Cron Run at " + DateTime.Now);
            return Content("Sent");
        }
    }
}