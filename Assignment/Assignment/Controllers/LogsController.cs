﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Assignment.Models;
using Google.Cloud.Datastore.V1;

namespace Assignment.Controllers
{
    public class LogsController : Controller
    {
        // GET: Datastore
        public ActionResult Index()
        {
            DatastoreDb db = DatastoreDb.Create("userlogdb");

            Query query = new Query("users");

            List<Log> myLogs = new List<Log>();
            foreach (Entity entity in db.RunQuery(query).Entities)
            {
                Log myLog = new Log();
                myLog.Email = entity["email"].StringValue;
                myLog.LastLoggedIn = entity["lastloggedin"].TimestampValue.ToDateTimeOffset().LocalDateTime;
                myLog.Id = entity.Key.ToString();

                myLogs.Add(myLog);
            }

            return View(myLogs);
        }

        public ActionResult Create()
        {
            DatastoreDb db = DatastoreDb.Create("userlogdb");

            Entity log = new Entity()
            {
                Key = db.CreateKeyFactory("users").CreateIncompleteKey(), //incompletekey : auto generated id
                ["email"] = User.Identity.Name,
                ["lastloggedin"] = DateTime.UtcNow,
                ["active"] = true

            };

            db.Insert(log);


            return Content("Operation done!");
        }
    }
}
