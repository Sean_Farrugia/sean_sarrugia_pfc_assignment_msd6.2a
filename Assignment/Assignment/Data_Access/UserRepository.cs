﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Web;

namespace Assignment.Data_Access
{
    public class UserRepository : ConnectionClass
    {
        public UserRepository() : base()
        { }

        public void AddUser(string email, string name)
        {
            string sql = "INSERT into users (email, fullname, lastloggedin, is_admin) values (@email, @name, @lastloggedin, false)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@name", name);
            cmd.Parameters.AddWithValue("@lastloggedin", DateTime.Now); ;


            MyConnection.Open();
            cmd.ExecuteNonQuery();
            MyConnection.Close();
        }

        public bool DoesEmailExist(string email)
        {
            string sql = "Select Count(*) from users where email = @email";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);

            MyConnection.Open();

            bool result = Convert.ToBoolean(cmd.ExecuteScalar());

            MyConnection.Close();

            return result;
        }

        public void UpdateLastLoggedIn(string email)
        {

            string sql = "update users set lastloggedin = @lastloggedin where email = @email";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@lastloggedin", DateTime.Now); ;

            MyConnection.Open();
            cmd.ExecuteNonQuery();
            MyConnection.Close();

        }
    }
}