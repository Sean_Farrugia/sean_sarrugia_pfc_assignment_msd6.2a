﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Google.Cloud.Kms.V1;
using System.IO;
using Google.Protobuf;
using System.Text;

namespace Assignment.Data_Access
{
    public class KeyRepository
    {
        public static string Encrypt(string plaintext)
        {
            KeyManagementServiceClient client = KeyManagementServiceClient.Create();
            //e.g. of key id: projects/progforthecloudt2020/locations/global/keyRings/pfckeyring001/cryptoKeys/pfckeys
            CryptoKeyName kn = CryptoKeyName.FromUnparsed(
                  new Google.Api.Gax.UnparsedResourceName("projects/cloudprogramming-267908/locations/global/keyRings/pfcKeyRing/cryptoKeys/pfcKeys")
                  );
            string cipher = client.Encrypt(kn, ByteString.CopyFromUtf8(plaintext)).Ciphertext.ToBase64();
            //since we are using Google, the encryption method involves converting the text to be encrypted and the cipher into a number of classes that are not
            //built-in in .NET so we need to adapt to the classes installed with the library from NuGet.

            return cipher;
        }
        public static string Decrypt(string cipher)
        {
            KeyManagementServiceClient client = KeyManagementServiceClient.Create();

            CryptoKeyName kn = CryptoKeyName.FromUnparsed(
                  new Google.Api.Gax.UnparsedResourceName("projects/cloudprogramming-267908/locations/global/keyRings/pfcKeyRing/cryptoKeys/pfcKeys")
                  );
            byte[] bytes = Convert.FromBase64String(cipher);
            string result = client.Decrypt(kn, ByteString.CopyFrom(bytes)).Plaintext.ToStringUtf8();

            return result; //change returned value.
        }
    }
}