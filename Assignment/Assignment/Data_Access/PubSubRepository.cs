﻿using Assignment.Models;
using Google.Cloud.PubSub.V1;
using Google.Protobuf;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.Json;
using System.Web;

namespace Assignment.Data_Access
{
    public class PubSubRepository
    {
        TopicName tn;
        SubscriptionName sn;
        public PubSubRepository()
        {
            tn = new TopicName("cloudprogramming-267908", "EmailTopic");  //A Queue/Topic will be created to hold the emails to be sent.  It will always have the same name DemoTopic, which you can change
            sn = new SubscriptionName("cloudprogramming-267908", "EmailSubscription");  //A Subscription will be created to hold which messages were read or not.  It will always have the same name DemoSubscription, which you can change
        }
        private Topic CreateGetTopic()
        {
            PublisherServiceApiClient client = PublisherServiceApiClient.Create();   //We check if Topic exists, if no we create it and return it
            TopicName tn = new TopicName("cloudprogramming-267908", "EmailTopic");
            try
            {
                return client.GetTopic(tn);
            }
            catch(Exception ex)
            {
                new LogsRepository().LogError(ex);
                return client.CreateTopic(tn);
            }


        }

        /// <summary>
        /// Publish method: uploads a message to the queue
        /// </summary>
        /// <param name="p"></param>
        public void AddToEmailQueue(Audio p)
        {
            PublisherServiceApiClient client = PublisherServiceApiClient.Create();
            var t = CreateGetTopic();


            string serialized = KeyRepository.Encrypt(JsonSerializer.Serialize(p, typeof(Audio)));
            List <PubsubMessage> messagesToAddToQueue = new List<PubsubMessage>(); // the method takes a list, so you can upload more than 1 message/item/product at a time
            PubsubMessage msg = new PubsubMessage();
            msg.Data = ByteString.CopyFromUtf8(serialized);

            messagesToAddToQueue.Add(msg);


            client.Publish(t.TopicName, messagesToAddToQueue); //committing to queue

        }


        private Subscription CreateGetSubscription()
        {
            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();  //We check if Subscription exists, if no we create it and return it

            try
            {
                return client.GetSubscription(sn);
            }
            catch(Exception ex)
            {
                new LogsRepository().LogError(ex);
                return client.CreateSubscription(sn, tn, null, 30);
            }

        }

        static bool mailSent = false;
        private static void SendCompletedCallback(object sender, AsyncCompletedEventArgs e)
        {
            // Get the unique identifier for this asynchronous operation.
            String token = (string)e.UserState;

            if (e.Cancelled)
            {
                Console.WriteLine("[{0}] Send canceled.", token);
            }
            if (e.Error != null)
            {
                Console.WriteLine("[{0}] {1}", token, e.Error.ToString());
            }
            else
            {
                Console.WriteLine("Message sent.");
            }
            mailSent = true;
        }

        public void DownloadEmailFromQueueAndSend()
        {
            SubscriberServiceApiClient client = SubscriberServiceApiClient.Create();
            
            var s = CreateGetSubscription(); //This must be called before being able to read messages from Topic/Queue
            var pullResponse = client.Pull(s.SubscriptionName, true, 1); //Reading the message on top; You can read more than just 1 at a time
            if (pullResponse != null)
            {
                string toDeserialize = KeyRepository.Decrypt(pullResponse.ReceivedMessages[0].Message.Data.ToStringUtf8()); //extracting the first message since in the previous line it was specified to read one at a time. if you decide to read more then a loop is needed
                Audio deserialized = JsonSerializer.Deserialize<Audio>(toDeserialize); //Deserializing since when we published it we serialized it
                StringBuilder sb = new StringBuilder();
                MailMessage mm = new MailMessage();  //Message on queue/topic will consist of a ready made email with the desired content, you can upload anything which is serializable
                
                mm.To.Add(new MailAddress(deserialized.audioreceiver)); 
                mm.From = new MailAddress("noreply.pfctransfer@gmail.com");
                mm.Subject = "New Audio In Inventory";
                mm.Body = "Name:" + deserialized.audioname + " Download Link: ";//+ deserialized.medialink;
                SmtpClient smtpClient = new SmtpClient();
                smtpClient.Port = 587;
                smtpClient.Host = "smtp.gmail.com";
                smtpClient.EnableSsl = true;
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new NetworkCredential("noreply.pfctransfer@gmail.com", "noreply12345678");
                smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtpClient.SendCompleted += new SendCompletedEventHandler(SendCompletedCallback);
                smtpClient.Send(mm);
                
                // Clean up.
                mm.Dispose();


                //Send Email with deserialized. Documentation: https://docs.microsoft.com/en-us/dotnet/api/system.net.mail.smtpclient?view=netframework-4.8

                List<string> acksIds = new List<string>();
                acksIds.Add(pullResponse.ReceivedMessages[0].AckId); //after the email is sent successfully you acknolwedge the message so it is confirmed that it was processed

                client.Acknowledge(s.SubscriptionName, acksIds.AsEnumerable());
            }

        }
    }
}