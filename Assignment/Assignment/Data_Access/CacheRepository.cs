﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Web;

namespace Assignment.Models
{
    public class CacheRepository
    {
        private IDatabase db;
        public CacheRepository()
        {
            // var connection = ConnectionMultiplexer.Connect("localhost"); //localhost if cache server is installed locally after downloaded from https://github.com/rgl/redis/downloads 
            // connection to your REDISLABS.com db as in the next line NOTE: DO NOT USE MY CONNECTION BECAUSE I HAVE A LIMIT OF 30MB...CREATE YOUR OWN ACCOUNT
            var connection = ConnectionMultiplexer.Connect("redis-11816.c228.us-central1-1.gce.cloud.redislabs.com:11816,password=Pgs1Nf8YwfiQ2fpMC9r5E0tCNtvyliPT"); //<< connection here should be to your redis database from REDISLABS.COM
            db = connection.GetDatabase();
        }

        /// <summary>
        /// store a list of products in cache
        /// </summary>
        /// <param name="products"></param>
        public void UpdateCache(List<Audio> audios)
        {
            if (db.KeyExists("audios") == true)
                db.KeyDelete("audios");

            string jsonString;
            jsonString = JsonSerializer.Serialize(audios); 

            db.StringSet("audios", jsonString);
        }
        /// <summary>
        /// Gets a list of products from cache
        /// </summary>
        /// <returns></returns>
        public List<Audio> GetaudiosFromCache()
        {
            if (db.KeyExists("audios") == true)
            {
                var audios = JsonSerializer.Deserialize<List<Audio>>(db.StringGet("audios"));
                return audios;
            }
            else
            {
                return new List<Audio>();
            }
        }
    }
}