﻿using Assignment.Models;
using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Assignment.Data_Access
{
    public class AudioRepository : ConnectionClass
    {
        public AudioRepository() : base() { }


        public void AddAudio(string audioname, string description, string email, string receiver, string link)
        {
            string sql = "INSERT into audiofiles (audioname, audiodesc, owner_fk, receiver, medialink) values (@audioname, @description, @email, @receiver, @link)";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@audioname", audioname);
            cmd.Parameters.AddWithValue("@description", description);
            cmd.Parameters.AddWithValue("@email", email);
            cmd.Parameters.AddWithValue("@receiver", receiver);
            cmd.Parameters.AddWithValue("@link", link);


            MyConnection.Open();
            cmd.ExecuteNonQuery();
            MyConnection.Close();
        }



        public List<Audio> GetAudios(string email)
        {
            string sql = "Select id, audioname, audiodesc, medialink from audiofiles where owner_fk=@email or receiver=@email";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@email", email);

            MyConnection.Open();
            List<Audio> results = new List<Audio>();

            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {
                    Audio audio = new Audio();
                    audio.id = reader.GetInt32(0);
                    audio.audioname = reader.GetString(1);
                    audio.audiodesc = reader.GetString(2);
                    audio.medialink = reader.GetString(3);
                    results.Add(audio);
                }
            }

            MyConnection.Close();

            return results;
        }



        public Audio GetAudio(int id)
        {
            string sql = "Select id, audioname, audiodesc, medialink from audiofiles where id=@id";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);

            MyConnection.Open();
            Audio audio = new Audio();

            using (var reader = cmd.ExecuteReader())
            {
                while (reader.Read())
                {

                    audio.id = reader.GetInt32(0);
                    audio.audioname = reader.GetString(1);
                    audio.audiodesc = reader.GetString(2);
                    audio.medialink = reader.GetString(3);
                }
            }

            MyConnection.Close();

            return audio;
        }



        public void DeleteAudio(int id)
        {
            string sql = "Delete from audiofiles where id = @id";

            NpgsqlCommand cmd = new NpgsqlCommand(sql, MyConnection);
            cmd.Parameters.AddWithValue("@id", id);

            bool connectionOpenedInThisMethod = false;

            if (MyConnection.State == System.Data.ConnectionState.Closed)
            {
                MyConnection.Open();
                connectionOpenedInThisMethod = true;
            }

            if (MyTransaction != null)
            {
                cmd.Transaction = MyTransaction; //to participate in the opened trasaction (somewhere else), assign the Transaction property to the opened transaction
            }
            cmd.ExecuteNonQuery();

            if (connectionOpenedInThisMethod == true)
            {
                MyConnection.Close();
            }
        }
    }
}