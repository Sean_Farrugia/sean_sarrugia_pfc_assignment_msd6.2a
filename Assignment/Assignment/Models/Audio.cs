﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Assignment.Models
{
    public class Audio
    {
        public int id { get; set; }
        public string audioname { get; set; }
        public string audiodesc { get; set; }
        public string audioowner { get; set; }
        public string audioreceiver { get; set; }
        public string medialink { get; set; }
    }
}